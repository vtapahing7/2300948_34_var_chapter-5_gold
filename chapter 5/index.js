const express = require("express");
const ejs = require("ejs");
const app = express();
const fs = require("fs");

app.use(express.static("public"));

app.set("view engine", "ejs");

app.get("/", function (req, res) {
  res.render("pages/index");
});

app.get("/suit", function (req, res) {
  res.render("pages/index-suit");
});

app.get("/welcome", function (req, res) {
  res.render("pages/welcome");
});

app.get("/login", (req, res) => {
  const userData = JSON.parse(fs.readFileSync("users.json", "utf8"));
  const { username, password } = req.query;

  const user = userData.users.find(
    (user) => user.username === username && user.password === password
  );

  if (user) {
    // Successful login
    res.redirect("/welcome");
  } else {
    // Invalid credentials
    res.render("pages/login", { error: "Invalid username or password" });
  }
});

app.listen(3000, () => {
  console.log("Server started on port 3000");
});
